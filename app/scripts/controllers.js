'use strict';

/* Controllers */

var controllers = angular.module('angularRestfulAuth');

/**
 *
 */
controllers.controller('remoteCtrl', ['$rootScope', '$scope', '$location', '$localStorage', '$http',
    function ($rootScope, $scope, $location, $localStorage, $http) {

        console.log('----remoteCtrl');

    }]);

/**
 *
 */
controllers.controller('otherCtrl', ['$rootScope', '$scope', '$location', '$localStorage', '$http',
    function ($rootScope, $scope, $location, $localStorage, $http) {

        console.log('----otherCtrl');

    }]);
