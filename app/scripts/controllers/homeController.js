/**
 *
 */
controllers.controller('homeCtrl', ['$rootScope', '$scope', '$location', '$localStorage', '$http',
    function ($rootScope, $scope, $location, $localStorage, $http) {

        $scope.exampleFormData = {};

        /**
         *
         * @param formData
         * @param params
         */
        var orderFormData = function (formData, params) {
            var keys = [],
                orderedKeys = [],
                data = [],
                orderedFormData = {},
                i;
            for(var key in formData) {
                keys.push(key);
                orderedKeys.push(key);
            };
            orderedKeys.sort(function (a, b) {
                return Number(a > b)
            });
            //TODO 此处需要递归优化， （1）排序层次，（2）递归层次
            for(i = 0;i < orderedKeys.length; i++) {
                if(orderedKeys.hasOwnProperty(i)) {
                    data.push(formData[orderedKeys[i]]);
                    orderedFormData[orderedKeys[i]] = formData[orderedKeys[i]]
                }
            }
            console.log('data :: ----' + data.join(','));
            console.log('orderedKeys :: ----' + orderedKeys.join(','));
            console.log('orderedFormData :: ----' + angular.toJson(orderedFormData));
        };

        $scope.save = function () {
            orderFormData($scope.exampleFormData, {});
            console.log('-----------' + angular.toJson($scope.exampleFormData));
        }

    }]);