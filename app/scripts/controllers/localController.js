var controllers = angular.module('angularRestfulAuth');

/**
 *
 */
controllers.controller('localCtrl', ['$rootScope', '$scope', '$location', '$localStorage', '$http',
    function ($rootScope, $scope, $location, $localStorage, $http) {

        $scope.ipAddress = '127.0.0.1';
        $scope.testUrlArr = [
            '/api/dataset/imm_ldap',
            '/api/function'
        ];
        $scope.data = {};
        $scope.status = {};
        $scope.headers = {};
        $scope.config = {};

        $scope.testGet = function (urlIndex, url) {
            var testUrl, params = {};
            if($scope.enableHttps) {
                testUrl = 'https://' + $scope.ipAddress + url;
            } else {
                testUrl = 'http://' + $scope.ipAddress + url;
            }

            $http.get(testUrl, params)
                .success(function (data, status, headers, config) {
                    $scope.data = data;
                    $scope.status = status;
                    $scope.headers = headers('Authorization');
                    $scope.config = config;
                })
                .error(function (data, status, headers, config) {
                    $scope.data = data;
                    $scope.status = status;
                    $scope.headers = headers('Content-Type');
                    $scope.config = config;
                });
        };

        $scope.testPost = function (urlIndex, url) {
            var testUrl, params = {};
            if($scope.enableHttps) {
                testUrl = 'https://' + $scope.ipAddress + url;
            } else {
                testUrl = 'http://' + $scope.ipAddress + url;
            }

            $http.post(testUrl, {'USER_UserModify':'1,USERID,,3,48,0,0,0,0,,1,192.168.0.1'})
                .success(function (data, status, headers, config) {
                    $scope.data = data;
                    $scope.status = status;
                    $scope.headers = headers('accept');
                    $scope.config = config;
                })
                .error(function (data, status, headers, config) {
                    $scope.data = data;
                    $scope.status = status;
                    $scope.headers = headers;
                    $scope.config = config;
                });
        };

    }]);