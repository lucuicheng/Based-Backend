/**
 *
 */
controllers.controller('systemCtrl', ['$rootScope', '$scope', '$http', '$sce',
    function ($rootScope, $scope, $http, $sce) {

        $scope.exampleFormData = {};

        /**
         *
         */
        $scope.initSystemStatus = function () {

            $scope.nginx = {};

            var installPathExisted = localStorage.getItem('nginx.installPath') == undefined;
            var confPathExisted = localStorage.getItem('nginx.confPath') == undefined;

            $scope.installPathEditable = installPathExisted;
            $scope.confPathEditable = confPathExisted;
            $scope.nginx.installPath = installPathExisted ? '' : localStorage.getItem('nginx.installPath');
            $scope.nginx.confPath = confPathExisted ? '' : localStorage.getItem('nginx.confPath');
        };

        $scope.saveNgPath = function () {

            if ($scope.nginx.installPath != '') {
                $scope.installPathEditable = false;
                localStorage.setItem('nginx.installPath', $scope.nginx.installPath);
            }

            if ($scope.nginx.confPath != '') {
                $scope.confPathEditable = false;
                localStorage.setItem('nginx.confPath', $scope.nginx.confPath);
            }

        };

        $scope.applyNgPath = function () {
            var params = angular.copy($scope.nginx);
            $scope.system = {};

            $http.post('http://localhost:3004/api/system/conf', params)
                .success(function (data, status, headers, config) {
                    var _data = data.result;
                    $scope.system.conf = $sce.trustAsHtml(_data.config);
                })
                .error(function (data, status, headers, config) {

                });

            $http.post('http://localhost:3004/api/system/processes', params)
                .success(function (data, status, headers, config) {
                    $scope.system.processes = data.result;
                })
                .error(function (data, status, headers, config) {

                });
        };

        $scope.startNginx = function () {
            var installPath = $scope.nginx.installPath;

            var params = {};
            params.installPath = installPath.substring(0, installPath.lastIndexOf('\\'));
            params.pname = installPath.substring(installPath.lastIndexOf('\\') + 1, installPath.lastIndexOf('.'));
            params.execute = 'start';

            $http.post('http://localhost:3004/api/system/processes/execute', params)
                .success(function (data, status, headers, config) {
                    if('success' == data.result) {
                        $scope.applyNgPath();
                    }
                })
                .error(function (data, status, headers, config) {

                });
        };

        $scope.stopNginx = function () {
            var installPath = $scope.nginx.installPath;

            var params = {};
            params.installPath = installPath.substring(0, installPath.lastIndexOf('\\'));
            params.pname = installPath.substring(installPath.lastIndexOf('\\') + 1, installPath.lastIndexOf('.'));
            params.execute = 'stop';

            $http.post('http://localhost:3004/api/system/processes/execute', params)
                .success(function (data, status, headers, config) {
                    if('success' == data.result) {
                        $scope.applyNgPath();
                    }
                })
                .error(function (data, status, headers, config) {

                });
        };
        
        $scope.reloadNginx = function () {
            var installPath = $scope.nginx.installPath;

            var params = {};
            params.installPath = installPath.substring(0, installPath.lastIndexOf('\\'));
            params.pname = installPath.substring(installPath.lastIndexOf('\\') + 1, installPath.lastIndexOf('.'));
            params.execute = 'reload';

            $http.post('http://localhost:3004/api/system/processes/execute', params)
                .success(function (data, status, headers, config) {
                    if('success' == data.result) {
                        $scope.applyNgPath();
                    }
                })
                .error(function (data, status, headers, config) {

                });
        };

        $scope.killAll = function (pname) {
            var installPath = $scope.nginx.installPath;
            pname = installPath.substring(installPath.lastIndexOf('\\') + 1, installPath.lastIndexOf('.'));

            $http.post('http://localhost:3004/api/system/processes/kill', {'pname':pname})
                .success(function (data, status, headers, config) {
                    if('success' == data.result) {
                        $scope.applyNgPath();
                    }
                })
                .error(function (data, status, headers, config) {

                });
        };

        $scope.killThis = function (pid) {
            $http.post('http://localhost:3004/api/system/processes/kill', {'pid':pid})
                .success(function (data, status, headers, config) {
                    if('success' == data.result) {
                        $scope.applyNgPath();
                    }
                })
                .error(function (data, status, headers, config) {

                });
        };
    }]);