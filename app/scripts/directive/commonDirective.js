'use strict';

angular.module('commonDirective', [])

    .directive('fileUpload', ['$parse', 'uiUploader', function ($parse, uiUploader) {
        return {
            restrict: 'A',
            scope: {
                fileName: '=',
                showValue: '=',
            },
            link: function ($scope, element, attrs, ngModel) {
                element.bind('change', function (e) {
                    uiUploader.removeAll();
                    var files = e.target.files;
                    uiUploader.addFiles(files);
                    $scope.fileName = uiUploader.getFiles()[0].name;
                    $scope.$apply();
                });
            }
        };
    }])

    .directive('fileUploadNow', ['$parse', 'uiUploader', function ($parse, uiUploader) {
        return {
            restrict: 'A',
            scope: {
                fileName: '=',
                fileType: '@',
                completeFun: '='
            },
            link: function ($scope, element, attrs) {
                $scope.uploadStatus = 0;//init default

                element.bind('change', function (e) {
                    uiUploader.removeAll();
                    var files = e.target.files;
                    uiUploader.addFiles(files);
                    $scope.fileName = uiUploader.getFiles()[0].name;
                    $scope.$apply();

                    uiUploader.startUpload({
                        'url': 'http://127.0.0.1:3004/api/system/execute',
                        'onProgress': function (file) {
                            var percentage = (file.loaded / uiUploader.getFiles()[0].size * 100).toFixed(0);
                            if (percentage >= 90) {
                                percentage = 90;
                            }
                            $scope.uploadPercentage = percentage;
                            $scope.$apply();
                        },
                        'onCompleted': function (file, data) {//file, data, status

                            data = angular.fromJson(data);
                            if (data.items != null && data.items[0] != null) {
                                $scope.resultStatus = true;

                                if (typeof $scope.completeFunParams != Object) {//complete fun post params
                                    $scope.completeFunParams = {};
                                }
                                $scope.completeFunParams.path = data.items[0].path;//get uploaded file's path

                                //After Upload Complete, do function, required complete fun use promise
                                $scope.completeFun($scope.completeFunParams).then(
                                    function (result) {
                                        //percentage to 100 and return init 0 TODO
                                        // $scope.uploadStatus = 0;
                                        // $scope.uploadPercentage = 100;
                                    }, function (err) {

                                    });

                            } else {
                                $scope.resultStatus = false;
                            }
                            $scope.$apply();
                        }
                    });

                });
            }
        };
    }]);