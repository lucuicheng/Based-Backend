var fs = require("fs");
var Q = require("q");

module.exports = function () {
    return {
        'read' : function (file, encoding) {
            var deferred = Q.defer();
            fs.readFile(file, encoding, function (err, data) {
                if (err) deferred.reject(err); // rejects the promise with `er` as the reason
                else deferred.resolve(data); // fulfills the promise with `data` as the value
            });
            return deferred.promise; // the promise is returned
        }
    }
};
