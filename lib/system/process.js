var Q = require("q");
var os = require("os");
var process = require("process");
var c_p = require('child_process');
var exec = c_p.exec;
var spawn = c_p.spawn;

var osType = function () {
    var result = {};
    result.platform = os.platform();//process.platform
    result.platform.indexOf('win') > -1
};

module.exports = function () {
    return {
        'taskList': function (processName) {
            var command = "tasklist";//TODO if linux or other OS
            var deferred = Q.defer();
            exec(command, function (err, stdout, stderr) {
                if (err) deferred.reject(err);
                else {
                    var processArr = [], process;
                    stdout.split('\n').filter(function (line) {
                        var p = line.trim().split(/\s+/), pname = p[0], pid = p[1];
                        if (pname.toLowerCase().indexOf(processName) >= 0 && parseInt(pid)) {
                            process = {};
                            process.name = pname;
                            process.id = pid;
                            processArr.push(process);
                        }
                    });
                    deferred.resolve(processArr);
                }
            });
            return deferred.promise;
        },
        'action': function (installPath, processName, option) {
            //TODO very maybe a little complex, need to use service or some others in OS
            option = option == undefined ? [] : option;
            var command = 'start /D ' + installPath + ' ' + processName + ' ' + option.join(' ');
            var deferred = Q.defer();
            exec(command, function (err, stdout, stderr) {
                if (err) deferred.reject(err);
                else {
                    deferred.resolve('success');
                }
            });
            /*var result = spawn(processName, []);
             result.stdout.on('data', function (data) {
             console.log('--------' + data);
             deferred.resolve('success');
             });

             result.stderr.on('data', function (err) {
             deferred.reject(err.toString("utf-8"));
             });
             result.on('exit', function (code) {
             console.log('Child exited with code ${code}');
             });*/

            return deferred.promise;
        },
        'kill': function (processTag) {
            //TODO very maybe a little complex, need to use service
            var command = "tskill " + processTag;
            var deferred = Q.defer();
            exec(command, function (err, stdout, stderr) {
                if (err) deferred.reject(err);
                else {
                    deferred.resolve('success');
                }
            });
            return deferred.promise;
        }
    }
};
