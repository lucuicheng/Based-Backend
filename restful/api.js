/**
 * Created by lucc1 on 2016/8/2.
 */
'use strict';

var login = require('./api/login');
var upload = require('./upload');
var driveInfo = require('./api/driveinfo');
var storageInfo = require('./api/storageinfo');
var countryinfo = require('./api/countryinfo');

var certUpload = require('./api/providers/certUpload');
var getBootOrder = require('./api/providers/get_boot_order');
var providersResult = require('./api/providers/result');
var immFod = require('./api/providers/imm_fod');
var adpater = require('./api/providers/adpater');
var datas = require('./api/providers/datas');
var heartbeats = require('./api/providers/heartbeats');

var functionResult = require('./api/function/result');
var raidAllDevices = require('./api/function/raid_alldevices');
var raidConf = require('./api/function/raid_conf');
var dataset = require('./api/dataset/datas');
var datasetResult = require('./api/dataset/result');

var system = require('./api/system');

module.exports = function (app) {
    login(app);
    upload(app);

    driveInfo(app);
    storageInfo(app);
    countryinfo(app);

    certUpload(app);
    getBootOrder(app);
    providersResult(app);
    immFod(app);
    adpater(app);
    datas(app);
	heartbeats(app);

    functionResult(app);
    raidAllDevices(app);
    raidConf(app);
    dataset(app);
    datasetResult(app);

    system(app);
};