'use strict';

var fs = require("fs");

module.exports = function (app) {
    app.get('/api/countryinfo', function (req, res) {
        fs.readFile("datas/country-codes.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData));
        });
    });
};

