/**
 * Created by lucc1 on 2016/8/8.
 */
'use strict';

var fs = require("fs");

module.exports = function (app) {

    app.get('/api/dataset/imm_status_power', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_status_power);
        });
    });

    app.get('/api/dataset/pwrmgmt', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            var param = req.query.params;
            param = param.split(',')[0];
            res.json(JSON.parse(fileData)[param]);
        });
    });

    /*------------------------------------------------------------------*/

    app.get('/api/dataset/imm_props_environ', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_props_environ);
        });
    });

    app.get('/api/dataset/fan-list', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).fan_list);
        });
    });

    app.get('/api/dataset/imm_status_power', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_status_power);
        });
    });

    app.get('/api/dataset/imm_properties', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_properties);
        });
    });

    /*------------------------------------------------------------------*/

    app.get('/api/dataset/imm_security', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).security);
        });
    });

    app.get('/api/dataset/imm_ipmi', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).ipmi);
        });
    });

    app.get('/api/dataset/imm_security_level', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).securityLevel);
        });
    });

    app.get('/api/dataset/imm_config', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).config);
        });
    });

    app.get('/api/dataset/imm_security_ssh_server', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).sshServe);
        });
    });

    /*----------------------------user--------------------------------------*/

    app.get('/api/dataset/imm_users', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_users);
        });
    });

    app.get('/api/dataset/imm_snmp', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_snmp);
        });
    });

    app.get('/api/dataset/imm_users_global', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_users_global);
        });
    });

    app.get('/api/dataset/imm_ldap', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_ldap);
        });
    });

    app.get('/api/dataset/imm_security_ldap_client', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_security_ldap_client);
        });
    });

    app.get('/api/dataset/imm_security_ldap_server', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_security_ldap_server);
        });
    });

    app.get('/api/dataset/imm_user_ssh_keys', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_user_ssh_keys);
        });
    });

    app.get('/api/dataset/imm_user_ssh_keys_update', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_user_ssh_keys_update);
        });
    });

    app.get('/api/dataset/imm_group_profiles', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_group_profiles);
        });
    });

    /*----------------------------event--------------------------------------*/

    app.get('/api/dataset/imm_user_ssh_keys_update', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_user_ssh_keys_update);
        });
    });

    app.get('/api/dataset/imm_group_profiles', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_group_profiles);
        });
    });

    app.get('/api/dataset/imm_user_ssh_keys_update', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_user_ssh_keys_update);
        });
    });

    app.get('/api/dataset/imm_group_profiles', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_group_profiles);
        });
    });

    /*-=-----------------------------------------------------*/
    app.get('/api/dataset/imm_general', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_general);
        });
    });

    app.get('/api/dataset/sys_info', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).sys_info);
        });
    });

    app.get('/api/dataset/sys_inventory', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).sys_inventory);
        });
    });

    app.get('/api/dataset/imm_processors', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_processors);
        });
    });

    app.get('/api/dataset/imm_memory', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_memory);
        });
    });

    app.get('/api/dataset/imm_power_supplies', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_power_supplies);
        });
    });

    app.get('/api/dataset/imm_props_hardware_com', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_props_hardware_com);
        });
    });

    app.get('/api/dataset/imm_system', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_system);
        });
    });

    app.get('/api/dataset/fan-list', function (req, res) {
        fs.readFile("datas/dataset.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).fan_list);
        });
    });
};