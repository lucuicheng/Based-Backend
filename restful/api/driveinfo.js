'use strict';
module.exports = function (app) {
    app.get('/api/driveinfo', function (req, res) {
        var data = {
            "driveCount": 3,
            "drivesInfo": [
                {
                    "driveCapacity": "20",
                    "driveName": "Drive 7",
                    "driveRole": "",
                    "driveType": "SAS",
                    "id": "2"
                },
                {
                    "driveCapacity": "20",
                    "driveName": "Drive 8",
                    "driveRole": "",
                    "driveType": "SAS",
                    "id": "3"
                }
            ]
        };
        res.json(data);
    });
};