'use strict';
module.exports = function (app) {
    app.get('/api/function/raid_alldevices', function (req, res) {
        var data = {
            "identifier": "storage.id",
            "items": [
                {
                    "controllerCount": 2,
                    "controllerInfo": [
                        {
                            "createModeFlag": 3,
                            "id": 1,
                            "name": "ServeRAID M5110e",
                            "pools": [
                                {
                                    "disks": [
                                        {
                                            "RAIDState": "Hot spare",
                                            "fruPartNo": "42C0201",
                                            "id": 0,
                                            "name": "Drive 1",
                                            "partNo": "43X0801",
                                            "serialNo": "D3A04301",
                                            "slotNo": 1,
                                            "type": "500GB 15K 12Gbps SAS 2.5\" HDD"
                                        },
                                        {
                                            "RAIDState": "Online",
                                            "fruPartNo": "42C0202",
                                            "id": 1,
                                            "name": "Drive 2",
                                            "partNo": "43X0802",
                                            "serialNo": "D3A04302",
                                            "slotNo": 2,
                                            "type": "500GB 10K 12Gbps SAS 2.5\" HDD"
                                        },
                                        {
                                            "RAIDState": "Dedicated Hot Spare",
                                            "fruPartNo": "42C0203",
                                            "id": 2,
                                            "name": "Drive 3",
                                            "partNo": "43X0803",
                                            "serialNo": "D3A04303",
                                            "slotNo": 3,
                                            "type": "512GB 15Gbps SAS 2.5\" SSD"
                                        }
                                    ],
                                    "freeCapacityStr": "0GB",
                                    "id": 0,
                                    "rdlvl": 4,
                                    "rdlvlstr": "RAID 5",
                                    "totalCapacityStr": "500GB",
                                    "uid": 0,
                                    "volumes": [
                                        {
                                            "capacity": 204800,
                                            "capacityStr": "50 GB",
                                            "id": 0,
                                            "name": "VD_0",
                                            "status": 3,
                                            "statusStr": "Optimal",
                                            "uid": 0
                                        },
                                        {
                                            "capacity": 204800,
                                            "capacityStr": "100 GB",
                                            "id": 1,
                                            "name": "VD_1",
                                            "status": 3,
                                            "statusStr": "Optimal",
                                            "uid": 0
                                        },
                                        {
                                            "capacity": 204800,
                                            "capacityStr": "150 GB",
                                            "id": 2,
                                            "name": "VD_2",
                                            "status": 3,
                                            "statusStr": "Optimal",
                                            "uid": 0
                                        },
                                        {
                                            "capacity": 307200,
                                            "capacityStr": "300 GB",
                                            "id": 3,
                                            "name": "VD_3",
                                            "status": 1,
                                            "statusStr": "Partially Degraded",
                                            "uid": 1
                                        }
                                    ]
                                },
                                {
                                    "disks": [
                                        {
                                            "RAIDState": "Online",
                                            "fruPartNo": "42C0204",
                                            "id": 3,
                                            "name": "Drive 4",
                                            "partNo": "43X0804",
                                            "serialNo": "D3A04304",
                                            "slotNo": 4,
                                            "type": "1TB 10K 12Gbps SATA 3.5\" HDD"
                                        },
                                        {
                                            "RAIDState": "Offline",
                                            "fruPartNo": "42C0205",
                                            "id": 4,
                                            "name": "Drive 5",
                                            "partNo": "43X0805",
                                            "serialNo": "D3A04305",
                                            "slotNo": 5,
                                            "type": "1.2TB 12K 15Gbps SATA 3.5\" HDD"
                                        }
                                    ],
                                    "freeCapacityStr": "0GB",
                                    "id": 1,
                                    "rdlvl": 2,
                                    "rdlvlstr": "RAID 1",
                                    "totalCapacityStr": "1GB",
                                    "uid": 1,
                                    "volumes": [
                                        {
                                            "capacity": 1024000,
                                            "capacityStr": "1000 GB",
                                            "id": 4,
                                            "name": "VD_2",
                                            "status": 3,
                                            "statusStr": "Optimal",
                                            "uid": 2
                                        }
                                    ]
                                }
                            ],
                            "serialNo": "23V04K",
                            "slotNo": 4,
                            "type": 0,
                            "unconfiguredDisks": [
                                {
                                    "RAIDState": "Offline",
                                    "fruPartNo": "42C0207",
                                    "iType": "SAS",
                                    "id": 1,
                                    "name": "Drive 1",
                                    "partNo": "43X0807",
                                    "serialNo": "D3A04307",
                                    "slotNo": 1,
                                    "type": "256GB 15Gbps SAS 2.5\" SSD"
                                },
                                {
                                    "RAIDState": "Offline",
                                    "fruPartNo": "42C0207",
                                    "iType": "SAS",
                                    "id": 2,
                                    "name": "Drive 2",
                                    "partNo": "43X0807",
                                    "serialNo": "D3A04307",
                                    "slotNo": 1,
                                    "type": "256GB 15Gbps SAS 2.5\" SSD"
                                },
                                {
                                    "RAIDState": "Offline",
                                    "fruPartNo": "42C0207",
                                    "iType": "SAS",
                                    "id": 3,
                                    "name": "Drive 3",
                                    "partNo": "43X0807",
                                    "serialNo": "D3A04307",
                                    "slotNo": 1,
                                    "type": "256GB 15Gbps SAS 2.5\" SSD"
                                },
                                {
                                    "RAIDState": "Offline",
                                    "fruPartNo": "42C0207",
                                    "iType": "SAS",
                                    "id": 4,
                                    "name": "Drive 4",
                                    "partNo": "43X0807",
                                    "serialNo": "D3A04307",
                                    "slotNo": 1,
                                    "type": "256GB 15Gbps SAS 2.5\" SSD"
                                },
                                {
                                    "RAIDState": "Offline",
                                    "fruPartNo": "42C0207",
                                    "iType": "SAS",
                                    "id": 5,
                                    "name": "Drive 5",
                                    "partNo": "43X0807",
                                    "serialNo": "D3A04307",
                                    "slotNo": 1,
                                    "type": "256GB 15Gbps SAS 2.5\" SSD"
                                }
                            ],
                            "volumeCount": 3,
                            "volumeCountStr": "3 volumes"
                        },
                        {
                            "createModeFlag": 3,
                            "id": 2,
                            "name": "ServeRAID M5210",
                            "pools": [
                                {
                                    "disks": [
                                        {
                                            "RAIDState": "Online",
                                            "fruPartNo": "42C0206",
                                            "id": 5,
                                            "name": "Drive 1",
                                            "partNo": "43X0806",
                                            "serialNo": "D3A04306",
                                            "slotNo": 1,
                                            "type": "512GB 12Gbps SAS 2.5\" SSD"
                                        }
                                    ],
                                    "freeCapacityStr": "312GB",
                                    "id": 2,
                                    "rdlvl": 1,
                                    "rdlvlstr": "RAID 0",
                                    "totalCapacityStr": "512GB",
                                    "uid": 2,
                                    "volumes": [
                                        {
                                            "capacity": 204800,
                                            "capacityStr": "200 GB",
                                            "id": 3,
                                            "name": "VD_4",
                                            "status": 2,
                                            "statusStr": "Degraded",
                                            "uid": 2
                                        }
                                    ]
                                }
                            ],
                            "serialNo": "38J15Q",
                            "slotNo": 9,
                            "type": 0,
                            "unconfiguredDisks": [
                                {
                                    "RAIDState": "JBOD",
                                    "fruPartNo": "42C0207",
                                    "iType": "SAS",
                                    "id": 6,
                                    "name": "Drive 6",
                                    "partNo": "43X0807",
                                    "serialNo": "D3A04307",
                                    "slotNo": 1,
                                    "type": "256GB 15Gbps SAS 2.5\" SSD"
                                },
                                {
                                    "RAIDState": "JBOD",
                                    "fruPartNo": "42C0207",
                                    "iType": "SAS",
                                    "id": 7,
                                    "name": "Drive 7",
                                    "partNo": "43X0807",
                                    "serialNo": "D3A04307",
                                    "slotNo": 1,
                                    "type": "256GB 15Gbps SAS 2.5\" SSD"
                                },
                                {
                                    "RAIDState": "Offline",
                                    "fruPartNo": "42C0207",
                                    "iType": "SAS",
                                    "id": 8,
                                    "name": "Drive 8",
                                    "partNo": "43X0807",
                                    "serialNo": "D3A04307",
                                    "slotNo": 1,
                                    "type": "256GB 15Gbps SAS 2.5\" SSD"
                                },
                                {
                                    "RAIDState": "JBOD",
                                    "fruPartNo": "42C0207",
                                    "iType": "SAS",
                                    "id": 9,
                                    "name": "Drive 9",
                                    "partNo": "43X0807",
                                    "serialNo": "D3A04307",
                                    "slotNo": 1,
                                    "type": "256GB 15Gbps SAS 2.5\" SSD"
                                },
                                {
                                    "RAIDState": "Offline",
                                    "fruPartNo": "42C0207",
                                    "iType": "SAS",
                                    "id": 10,
                                    "name": "Drive 10",
                                    "partNo": "43X0807",
                                    "serialNo": "D3A04307",
                                    "slotNo": 1,
                                    "type": "256GB 15Gbps SAS 2.5\" SSD"
                                }
                            ],
                            "volumeCount": 3,
                            "volumeCountStr": "1 volume"
                        }
                    ],
                    "disks": [
                        {
                            "capacityStr": "500GB",
                            "fruPartNo": "42C0201",
                            "healthStatus": 2,
                            "id": 0,
                            "manufacture": "Seagate",
                            "partNo": "43X0801",
                            "serialNo": "D3A04301",
                            "slotNo": 1,
                            "type": "500GB 15K 12Gbps SAS 2.5\" HDD",
                            "uuid": "4cac33d1f4d045e4a9498002d1105e1"
                        },
                        {
                            "capacityStr": "500GB",
                            "fruPartNo": "42C0202",
                            "healthStatus": 1,
                            "id": 1,
                            "manufacture": "West Digital",
                            "partNo": "43X0802",
                            "serialNo": "D3A04302",
                            "slotNo": 2,
                            "type": "500GB 10K 12Gbps SAS 2.5\" HDD",
                            "uuid": "4cac33d1f4d045e4a9498002d1105e2"
                        },
                        {
                            "capacityStr": "512GB",
                            "fruPartNo": "42C0203",
                            "healthStatus": 0,
                            "id": 2,
                            "manufacture": "Toshiba",
                            "partNo": "43X0803",
                            "serialNo": "D3A04303",
                            "slotNo": 3,
                            "type": "512GB 15Gbps SAS 2.5\" SSD",
                            "uuid": "4cac33d1f4d045e4a9498002d1105e3"
                        },
                        {
                            "capacityStr": "512GB",
                            "fruPartNo": "42C0206",
                            "healthStatus": 2,
                            "id": 5,
                            "manufacture": "Seagate",
                            "partNo": "43X0806",
                            "serialNo": "D3A04306",
                            "slotNo": 4,
                            "type": "512GB 12Gbps SAS 2.5\" SSD",
                            "uuid": "4cac33d1f4d045e4a9498002d1105e4"
                        },
                        {
                            "capacityStr": "256GB",
                            "fruPartNo": "42C0207",
                            "healthStatus": 1,
                            "id": 6,
                            "manufacture": "Seagate",
                            "partNo": "43X0807",
                            "serialNo": "D3A04307",
                            "slotNo": 5,
                            "type": "256GB 15Gbps SAS 2.5\" SSD",
                            "uuid": "4cac33d1f4d045e4a9498002d1105e5"
                        }
                    ],
                    "storage.id": "controllers",
                    "totalCapacityStr": "700GB",
                    "volumes": [
                        {
                            "capacityStr": "200GB",
                            "drives": "Drive1,Drive2",
                            "id": 0,
                            "name": "VD_0",
                            "rdlvlstr": "RAID 1",
                            "status": 3,
                            "statusStr": "Optimal"
                        },
                        {
                            "capacityStr": "300GB",
                            "drives": "Drive1,Drive2",
                            "id": 1,
                            "name": "VD_1",
                            "rdlvlstr": "RAID 1",
                            "status": 1,
                            "statusStr": "Partially Degraded"
                        },
                        {
                            "capacityStr": "200GB",
                            "drives": "Drive3,Drive4",
                            "id": 3,
                            "name": "VD_3",
                            "rdlvlstr": "RAID 0",
                            "status": 2,
                            "statusStr": "Degraded"
                        }
                    ]
                }
            ]
        };
        /*var data = {
         "identifier": "storage.id",
         "items": [
         {
         "controllerCount": 0,
         "controllerInfo": [],
         "disks": [],
         "enclosures": [],
         "storage.id": "controllers",
         "totalCapacityStr": "0GB",
         "unmanagedDisks": [],
         "volumes": []
         }
         ]
         };*/
        res.json(data);
    });
};
