'use strict';
module.exports = function (app) {
    var datas = {
        "raidlink_CheckConfisValid": {
            "items": [
                {
                    "errcode": 0,
                    "freeCapacity": 1906394,
                    "ssdCacheBlock": 181
                }
            ]
        },
        "raidlink_CheckGrpisMatched": {
            "items": [
                {
                    "errcode": 0
                }
            ]
        },
        "raidlink_CheckHsisMatched": {
            "items": [
                {
                    "errcode": 0
                }
            ]
        },
        "raidlink_GetAllowedDiskAct": {
            "items": [
                {
                    "bDedicatedHS": 1,
                    "bGlobalHS": 1,
                    "bGoodTojbod": 0,
                    "bJbodTogood": 0,
                    "bMissing": 0,
                    "bOffline": 1,
                    "bOnline": 0,
                    "bPFRemoval": 1,
                    "bRemoveHS": 0,
                    "bReusable": 0,
                    "bUnconfbad": 1,
                    "bUnconfgood": 1
                }
            ]
        },
        "raidlink_GetArrayListEx": {
            "items": [
                {
                    "id": 0,
                    "uid": 0,
                    "rdlvl": 4,
                    "rdlvlstr": "RAID 5",
                    "hole": 0,
                    "freeCapacity": 102400,
                    "freeCapacityStr": "100GB",
                    "totalCapacity": 285148,
                    "totalCapacityStr": "500GB"
                },
                {
                    "id": 1,
                    "uid": 1,
                    "rdlvl": 2,
                    "rdlvlstr": "RAID 1",
                    "hole": 1,
                    "freeCapacity": 204800,
                    "freeCapacityStr": "200GB",
                    "totalCapacity": 285148,
                    "totalCapacityStr": "1000GB"
                },
                {
                    "id": 2,
                    "uid": 2,
                    "rdlvl": 2,
                    "rdlvlstr": "RAID 2",
                    "hole": 1,
                    "freeCapacity": 307200,
                    "freeCapacityStr": "300GB",
                    "totalCapacity": 285148,
                    "totalCapacityStr": "1000GB"
                },
                {
                    "id": 3,
                    "uid": 2,
                    "rdlvl": 2,
                    "rdlvlstr": "RAID 13",
                    "hole": 1,
                    "freeCapacity": 256000,
                    "freeCapacityStr": "250GB",
                    "totalCapacity": 285148,
                    "totalCapacityStr": "1000GB"
                }
            ]
        },
        "raidlink_GetArrayListForDHS": {
            "items": [
                {
                    "freeCapacityStr": "0GB",
                    "id": 0,
                    "rdlvl": 4,
                    "rdlvlstr": "RAID 5",
                    "totalCapacityStr": "500GB",
                    "uid": 0
                },
                {
                    "freeCapacityStr": "0GB",
                    "id": 1,
                    "rdlvl": 2,
                    "rdlvlstr": "RAID 1",
                    "totalCapacityStr": "1GB",
                    "uid": 3
                }
            ]
        },
        "raidlink_GetDisksToConf": {
            "items": [
                {
                    "supported_disklist": [
                        {
                            "capacityStr": "2000.399GB",
                            "iType": "SAS",
                            "id": 1,
                            "name": "Drive 255",
                            "sen": 0,
                            "slotNo": 255
                        },
                        {
                            "capacityStr": "2000.399GB",
                            "iType": "SAS",
                            "id": 2,
                            "name": "Drive 31",
                            "sen": 0,
                            "slotNo": 31
                        },
                        {
                            "capacityStr": "2000.399GB",
                            "iType": "SAS",
                            "id": 3,
                            "name": "Drive 28",
                            "sen": 0,
                            "slotNo": 28
                        },
                        {
                            "capacityStr": "2000.399GB",
                            "iType": "SAS",
                            "id": 4,
                            "name": "Drive 255",
                            "sen": 0,
                            "slotNo": 255
                        }
                    ],
                    "supported_raidlvl": [
                        {
                            "maxSpan": 1,
                            "minPd": 1,
                            "rdlvl": 1,
                            "rdlvlstr": "RAID 0"
                        },
                        {
                            "maxSpan": 1,
                            "minPd": 2,
                            "rdlvl": 2,
                            "rdlvlstr": "RAID 1"
                        },
                        {
                            "maxSpan": 1,
                            "minPd": 3,
                            "rdlvl": 4,
                            "rdlvlstr": "RAID 5"
                        },
                        {
                            "maxSpan": 2,
                            "minPd": 2,
                            "rdlvl": 32,
                            "rdlvlstr": "RAID 10"
                        },
                        {
                            "maxSpan": 4,
                            "minPd": 1,
                            "rdlvl": 512,
                            "rdlvlstr": "RAID 00"
                        }
                    ]
                }
            ]
        },
        "raidlink_GetDefaultVolProp": {
            "items": [
                {
                    "ap": 0,
                    "blksz": 512,
                    "capacity": 0,
                    "cpio": 0,
                    "cpra": 2,
                    "cpwb": 0,
                    "dcp": 0,
                    "dftpp": 0,
                    "initstate": 0,
                    "maxstrip": 11,
                    "minstrip": 7,
                    "name": "VD",
                    "stripsize": 11
                }
            ]
        },
        "raidlink_GetForeignCount": {
            "items": [
                {
                    "foreignDiskCount": 5
                }
            ]
        },
        "raidlink_GetVolProp": {
            "items": [
                {
                    "id": 1,
                    "name": "VD_1",
                    "capacity": 204800,
                    "blksz": 512,
                    "stripsize": 1,
                    "minstrip": 0,
                    "maxstrip": 0,
                    "cpra": 1,
                    "cpwb": 1,
                    "cpio": 1,
                    "ap": 0,
                    "dcp": 0,
                    "nobginit": 0,
                    "curpp": 0
                }
            ]
        }
    };

    app.get('/api/function/raid_conf', function (req, res) {
        var data;
        for (var key in datas) {
            if (req.query.params.indexOf(key) > -1) {
                data = datas[key];
            }
        }
        res.json(data);
    });
};