'use st rict';

var fs = require("fs");

module.exports = function (app) {
    app.get('/api/dataset/imm_adapters', function (req, res) {
        fs.readFile("datas/adapter.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData));
        });
    });
};