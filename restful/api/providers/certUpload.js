'use st rict';

var fs = require("fs");

module.exports = function (app) {

    app.post('/api/providers/cert_upload', function (req, res) {
        fs.readFile("datas/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).cert_upload);
        });
    });
    
};