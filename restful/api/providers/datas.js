'use st rict';

var fs = require("fs");

module.exports = function (app) {
    app.get('/api/providers/locationled', function (req, res) {
        fs.readFile("datas/providers/datas.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).locationled);
        });
    });

    app.post('/api/providers/ffdc', function (req, res) {
        fs.readFile("datas/providers/datas.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).ffdc);
        });
    });

    app.post('/api/providers/sshkey_upload', function (req, res) {
        fs.readFile("datas/providers/datas.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).sshkey_upload);
        });
    });


    app.get('/api/providers/bootmode', function (req, res) {
        fs.readFile("datas/providers/datas.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).bootmode_get);
        });
    });

    app.post('/api/providers/bootmode', function (req, res) {
        fs.readFile("datas/providers/datas.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).bootmode_post);
        });
    });

};