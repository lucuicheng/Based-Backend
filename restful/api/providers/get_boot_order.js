'use st rict';

var fs = require("fs");

module.exports = function (app) {

    app.get('/api/providers/get_boot_order', function (req, res) {
        fs.readFile("datas/boot_order.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData));
        });
    });

};