'use st rict';

var fs = require("fs");

module.exports = function (app) {
    app.get('/api/providers/heartbeats', function (req, res) {
        fs.readFile("datas/heartbeats.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData));
        });
    });
};