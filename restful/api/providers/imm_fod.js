'use st rict';

var fs = require("fs");

module.exports = function (app) {

    app.get('/api/providers/imm_fod', function (req, res) {
        fs.readFile("datas/imm_fod.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData));
        });
    });

    app.post('/api/providers/imm_fod', function (req, res) {
        fs.readFile("datas/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            var data;
            for (var key in req.body) {
                data = JSON.parse(fileData)[key];
            }
            res.json(data);
        });
    });

};