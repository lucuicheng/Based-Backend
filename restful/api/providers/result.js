'use strict';

var fs = require("fs");
var count = 0;
module.exports = function (app) {

    app.post('/api/providers/set_boot_order', function (req, res) {
        fs.readFile("datas/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).set_boot_order);
        });
    });

    app.get('/api/providers/get_one_time_boot', function (req, res) {
        fs.readFile("datas/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).get_one_time_boot);
        });
    });

    app.post('/api/providers/set_one_time_boot', function (req, res) {
        fs.readFile("datas/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).set_one_time_boot);
        });
    });

    app.post('/api/providers/poweraction', function (req, res) {
        fs.readFile("datas/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).poweraction);
        });
    });

    app.get('/api/providers/xccenvinfo', function (req, res) {
        fs.readFile("datas/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).xccenvinfo);
        });
    });

    app.get('/api/providers/sessioninfo', function (req, res) {
        fs.readFile("datas/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).sessioninfo);
        });
    });

    app.post('/api/providers/sessioninfo', function (req, res) {
        fs.readFile("datas/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).sessioninfo_post);
        });
    });

    /*------------------------fmupdate------------------------*/

    app.post('/api/providers/fwupdate', function (req, res) {
        fs.readFile("datas/fwupdate/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            var data;
            for (var key in req.body) {
                data = JSON.parse(fileData)[key];
            }
            res.json(data);
        });
    });

    app.get('/upload/progress', function (req, res) {
        fs.readFile("datas/fwupdate/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).progress);
        });
    });

    app.get('/api/dataset/imm_firmware_success', function (req, res) {
        fs.readFile("datas/fwupdate/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_firmware_success);
        });
    });

    app.get('/api/dataset/imm_firmware_update', function (req, res) {
        fs.readFile("datas/fwupdate/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_firmware_update);
        });
    });

    app.get('/api/dataset/imm_firmware_progress', function (req, res) {
        fs.readFile("datas/fwupdate/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            var percent = ( count++ ) * 5;
            if(percent >= 100) {
                percent = 100;
            }
            var result = JSON.parse(fileData).imm_firmware_progress;
            result.items[0].action_percent_complete = percent;
            res.json(result);
        });
    });

    app.get('/api/dataset/reset', function (req, res) {
        count = 0;
        res.json({'result':'success','count':count});
    });

    app.get('/api/function/adapter_update', function (req, res) {
        fs.readFile("datas/fwupdate/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            var param = req.query.params;
            param = param.split(',')[0];
            res.json(JSON.parse(fileData)[param]);
        });
    });
    
    app.get('/api/dataset/imm_firmware', function (req, res) {
        fs.readFile("datas/fwupdate/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_firmware);
        });
    });

    app.post('/api/providers/imm_reset', function (req, res) {
        fs.readFile("datas/fwupdate/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).imm_reset);
        });
    });

    app.get('/api/providers/logout', function (req, res) {
        fs.readFile("datas/fwupdate/result.json", function (error, fileData) {
            if (error) {
                console.log('read err : ' + error);
            }
            res.json(JSON.parse(fileData).logout);
        });
    });
   
    
};