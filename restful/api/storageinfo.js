'use strict';
module.exports = function (app) {
    app.get('/api/storageinfo', function (req, res) {
        var data = {
            "poolCount": 3,
            "storagePoolsInfo": [
                {
                    "freeCapacity": "100",
                    "id": "1",
                    "poolName": "Storage Pool 1",
                    "raidState": "RAID 0",
                    "totalCapacity": "400"
                },
                {
                    "freeCapacity": "20",
                    "id": "2",
                    "poolName": "Storage Pool 2",
                    "raidState": "RAID 0",
                    "totalCapacity": "400"
                },
                {
                    "freeCapacity": "40",
                    "id": "3",
                    "poolName": "Storage Pool 3",
                    "raidState": "RAID 0",
                    "totalCapacity": "400"
                }
            ]
        };
        res.json(data);
    });
};