'use strict';

var file = require('../../lib/common/file');
var process = require('../../lib/system/process');

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

module.exports = function (app) {

    app.post('/api/system/conf', function (req, res) {
        var result = {};
        var param = req.body.confPath;
        //get ngnix config
        if (typeof param != 'undefined' && param != '') {

            file().read(param)
                .then(function (data) {
                    result.config = data.toString("utf-8");
                    res.json({'result': result});

                }, function (error) {
                    console.error(error)
                });
        }
    });

    app.post('/api/system/processes', function (req, res) {
        var result = {};
        //get ngnix status
        var isEmpty = function (value) {//TODO Add to lib
            return typeof value == 'undefined' || value == '';
        };

        var installPath = req.body.installPath;
        var pname = installPath.substr(installPath.lastIndexOf('\\') + 1, installPath.lastIndexOf('.'));
        if (!isEmpty(pname)) {
            process().taskList(pname).then(function (data) {
                result.processes = data;
                result.count = data.length;
                res.json({'result': result});
            }, function (error) {
                console.error(error)
            });
        }
    });

    app.post('/api/system/processes/execute', function (req, res) {
        var result = {};
        //get ngnix status
        var isEmpty = function (value) {//TODO Add to lib
            return typeof value == 'undefined' || value == '';
        };

        var installPath = req.body.installPath,
            execute = req.body.execute,
            pname = req.body.pname,
            option;

        if (!isEmpty(pname) && !isEmpty(execute)) {
            if ('start' == execute) {//TODO use switch
                option = []
            }
            if ('stop' == execute) {
                option = ['-s', 'stop'];
            }
            if ('reload' == execute) {
                option = ['-s', 'reload'];
            }
            process().action(installPath, pname, option).then(function (data) {
                res.json({'result': data});
            }, function (error) {
                console.error(error)
            });
        }
    });

    app.post('/api/system/processes/kill', function (req, res) {
        var result = {};
        //get ngnix status
        var isEmpty = function (value) {//TODO Add to lib
            return typeof value == 'undefined' || value == '' || tag == null;
        };

        var pid = req.body.pid,
            pname = req.body.pname,
            tag = '';
        
        if(isEmpty(pid) || isEmpty(pname)) {
            tag += isEmpty(pid) ? '' : pid;
            tag += isEmpty(pname) ? '' : pname;
        }

        if (!isEmpty(tag)) {
            process().kill(tag).then(function (data) {
                res.json({'result': data});
            }, function (error) {
                console.error(error)
            });
        }
    });

    app.post('/api/system/upload', multipartMiddleware, function (req, res) {
        var result = {};
        //get ngnix status
        result.file = req.files;
        res.json({'result': result});
    });

};

